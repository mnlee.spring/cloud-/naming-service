<a href="https://mnlee.gitlab.io/portfolio/#gitlab">Back to Margaret Lee's Portfolio</a>

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Execution](#execution)
* [Testing](#testing)


## General info
* Naming Server -- allows services to find and communicate with each other without hard coding hostname and port.
	
## Technologies
Project is created with:
* Spring Boot version: 2.1.2
* Java version: 1.8
* Maven 3.3.9
* Netflix Eureka Naming Server
	
## Setup
* Create project using https://start.spring.io/ or Spring Boot CLI with dependencies
-- Eureka Server, Cofig Client, Actuator


### Steps
* Add contents to
	- application.properties
	- NamingServerApplication.java
		
* Register all the services, on each of the service add:
	- Add eureka.client.service-url.default-zone=http://localhost:8761/eureka to application.properties
	- Add to pom.xml for a service registry (Eureka Server)
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
		</dependency>
	- Add to pom.xml for a REST service which registers itself at the registry (Eureka Client) 
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
		</dependency>
	- Add to main App:
		@EnableDiscoveryClient


## Execution
* localhost:8761
	
## Testing
* browser





 